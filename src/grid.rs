use core::ops::{Index, IndexMut};

use std::collections::VecDeque;

use image::GrayImage;
use ndarray::Array2;
use rand::prelude::*;

use crate::tiles::{TileTypes, Tiles};

pub struct PropCtx {
    queue: VecDeque<(usize, usize)>,
    visited: Array2<bool>,
}

#[derive(Clone)]
pub struct Grid {
    types: TileTypes,
    data: Array2<Tiles>,
}

impl Index<(usize, usize)> for Grid {
    type Output = Tiles;

    fn index(&self, at: (usize, usize)) -> &Tiles {
        &self.data[at]
    }
}
impl IndexMut<(usize, usize)> for Grid {
    fn index_mut(&mut self, at: (usize, usize)) -> &mut Tiles {
        &mut self.data[at]
    }
}

impl Grid {
    pub fn new(types: TileTypes, width: usize, height: usize) -> Grid {
        let tile_count = types.count();
        let data = Array2::from_shape_fn((width, height), |_| Tiles::ALL.truncate(tile_count));
        Grid { types, data }
    }

    pub fn from_stencil(types: TileTypes, img: &GrayImage, stencil_all: bool) -> Grid {
        let tile_count = types.count();
        let (img_width, img_height) = img.dimensions();
        let (width, height) = (img_width as usize, img_height as usize);
        let mut grid = Grid::new(types, width, height);
        let mut pc = grid.make_propagation_context();
        let stencil_filled = if stencil_all {
            Tiles::ALL.truncate(tile_count)
        } else {
            Tiles::ALL.truncate(tile_count).without(Tiles::singleton(0))
        };
        for iy in 0..height {
            for ix in 0..width {
                let pixel = img.get_pixel(ix as u32, iy as u32);
                if pixel.0[0] == 0 {
                    grid.data[(ix, iy)] = stencil_filled;
                } else {
                    grid.data[(ix, iy)] = Tiles::singleton(0);
                }
            }
        }
        for iy in 0..height {
            for ix in 0..width {
                grid.propagate(&mut pc, (ix, iy));
            }
        }
        grid
    }

    pub fn dim(&self) -> (usize, usize) {
        self.data.dim()
    }

    pub fn make_propagation_context(&self) -> PropCtx {
        let (width, height) = self.data.dim();
        PropCtx {
            queue: VecDeque::new(),
            visited: Array2::from_shape_fn((width, height), |_| false),
        }
    }

    fn propagate(&mut self, ctx: &mut PropCtx, at: (usize, usize)) {
        let (width, height) = self.data.dim();
        ctx.queue.clear();
        ctx.visited.fill(false);
        ctx.queue.push_back(at);
        ctx.visited[at] = true;
        while let Some((x, y)) = ctx.queue.pop_front() {
            let tile = self.data[(x, y)];
            let mut acceptable = [Tiles::NONE; 4];
            for idx in tile.iter() {
                for edge_idx in 0..4 {
                    let tt = &self.types[idx];
                    let ea = tt.acceptable[edge_idx];
                    acceptable[edge_idx] = acceptable[edge_idx].union(ea);
                }
            }
            if y > 0 {
                let xy = (x, y - 1);
                let old = self.data[xy];
                let new = old.intersect(acceptable[0]);
                if old != new {
                    self.data[xy] = new;
                    ctx.queue.push_back(xy);
                }
            }
            if x < width - 1 {
                let xy = (x + 1, y);
                let old = self.data[xy];
                let new = old.intersect(acceptable[1]);
                if old != new {
                    self.data[xy] = new;
                    ctx.queue.push_back(xy);
                }
            }
            if y < height - 1 {
                let xy = (x, y + 1);
                let old = self.data[xy];
                let new = old.intersect(acceptable[2]);
                if old != new {
                    self.data[xy] = new;
                    ctx.queue.push_back(xy);
                }
            }
            if x > 0 {
                let xy = (x - 1, y);
                let old = self.data[xy];
                let new = old.intersect(acceptable[3]);
                if old != new {
                    self.data[xy] = new;
                    ctx.queue.push_back(xy);
                }
            }
        }
    }

    pub fn collapse_at<R: Rng>(
        &mut self,
        pc: &mut PropCtx,
        rng: &mut R,
        (x, y): (usize, usize),
    ) -> anyhow::Result<bool> {
        let idx_opt = self.data[(x, y)].random_idx(rng);
        let idx = match idx_opt {
            Some(idx) => idx,
            None => anyhow::bail!("cannot collapse, no options to collapse to"),
        };
        let new_tile = Tiles::singleton(idx);
        if self.data[(x, y)] != new_tile {
            self.data[(x, y)] = new_tile;
            self.propagate(pc, (x, y));
            Ok(true)
        } else {
            Ok(false)
        }
    }
    
    pub fn least_entropy_cell(&self, rng: &mut impl Rng) -> Option<(usize, usize)> {
        let (width, height) = self.data.dim();
        let mut candidates = Vec::new();
        let mut min_entropy = usize::MAX;
        for iy in 0..height {
            for ix in 0..width {
                let cell = self.data[(ix, iy)];
                let len = cell.len();
                if len > 1 && len < min_entropy {
                    min_entropy = len;
                    candidates.clear();
                    candidates.push((ix, iy));
                }
            }
        }
        candidates.choose(rng).cloned()
    }
}
