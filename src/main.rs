use std::{collections::VecDeque, str::FromStr};

use anyhow::anyhow;
use ndarray::Array2;
use structopt::StructOpt;
use rand::Rng;

mod grid;
mod tiles;
mod tileset;

use grid::Grid;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum CollapseSchedule {
    LeastEntropy,
    OffsetScan,
    Quadtree,
}

impl FromStr for CollapseSchedule {
    type Err = anyhow::Error;

    fn from_str(text: &str) -> anyhow::Result<CollapseSchedule> {
        match text {
            "least-entropy" | "le" => Ok(CollapseSchedule::LeastEntropy),
            "offset-scan" | "os" => Ok(CollapseSchedule::OffsetScan),
            "quadtree" | "qt" => Ok(CollapseSchedule::Quadtree),
            _ => Err(anyhow!("failed to parse collapse schedule")),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Collapser {
    LeastEntropy,
    Points(Vec<(usize, usize)>),
}

impl Collapser {
    pub fn least_entropy() -> Collapser {
        Collapser::LeastEntropy
    }
    
    pub fn offset_scan(width: usize, height: usize) -> Collapser {
        let mut points = Vec::with_capacity(width * height);
        let off = 2;
        let offsets = [(0, 0), (1, 1), (1, 0), (0, 1)];
        for (xo, yo) in offsets {
            for iy in (0..height).step_by(off) {
                for ix in (0..width).step_by(off) {
                    points.push((ix + xo, iy + yo));
                }
            }
        }
        points.reverse();
        Collapser::Points(points)
    }
    
    pub fn quadtree(width: usize, height: usize) -> Collapser {
        let mut points = Vec::with_capacity(width * height);
        let mut visited = Array2::from_shape_fn((width, height), |_| false);
        let mut nodes = VecDeque::new();
        nodes.push_back((0, 0, width, height));
        while let Some((nx, ny, nw, nh)) = nodes.pop_front() {
            if !visited[(nx, ny)] {
                visited[(nx, ny)] = true;
                points.push((nx, ny));
            }
            let hw = nw / 2;
            let hh = nh / 2;
            if nw > 1 && nh > 1 {
                nodes.push_back((nx, ny, hw, hh));
                nodes.push_back((nx + hw, ny + hh, hw, hh));
                nodes.push_back((nx + hw, ny, hw, hh));
                nodes.push_back((nx, ny + hh, hw, hh));
            }
        }
        points.reverse();
        Collapser::Points(points)
    }
    
    pub fn next(&mut self, rng: &mut impl Rng,  grid: &Grid) -> Option<(usize, usize)> {
        match self {
            Collapser::LeastEntropy => {
                grid.least_entropy_cell(rng)
            }
            Collapser::Points(points) => {
                points.pop()
            }
        }
    }
}

#[derive(StructOpt)]
struct Opt {
    /// Path to the tileset
    #[structopt(short = "t", long = "tileset")]
    tileset: String,
    /// Path to the image to use to fill tiles that haven't fully collapsed
    #[structopt(short = "f", long = "fill")]
    fill: String,
    /// Path to the stencil, if any, mutually exclusive with dim
    #[structopt(short = "s", long = "stencil")]
    stencil: Option<String>,
    /// Dimensions of the output image (example: 64x64)
    #[structopt(short = "d", long = "dimensions")]
    dim: Option<String>,
    /// Collapse schedule to use (options: least-entropy (short: le, default), offset-scan (short: os), quadtree (short: qt))
    #[structopt(short = "c", long = "schedule", default_value = "least-entropy")]
    schedule: CollapseSchedule,
    /// Whether to allow the filled in part of the stencil to be tile 0
    #[structopt(short = "a", long = "stencil-all")]
    stencil_all: bool,
    /// Whether to output frames to frames/xxxx.png
    #[structopt(short = "r", long = "output-frames")]
    output_frames: bool,
    /// Whether to retry on failure
    #[structopt(short = "k", long = "retry")]
    retry: bool,
    /// Whether to enable verbose output
    #[structopt(short = "v", long = "verbose")]
    verbose: bool,
    /// Output image
    #[structopt(short = "o", long = "output")]
    out: String,
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();
    let fill = image::open(&opt.fill)?.to_rgba8();
    let stencil = if let Some(stencil_path) = &opt.stencil {
        Some(image::open(stencil_path)?.to_luma8())
    } else {
        None
    };
    let tileset = tileset::Tileset::from_file(&opt.tileset)?;
    let mut rng = rand::thread_rng();
    let mut attempt = 0;
    loop {
        eprintln!("attempt {attempt}");
        let types = tileset.make_tile_types();
        let mut grid = if let Some(stencil_img) = &stencil {
            Grid::from_stencil(types, stencil_img, opt.stencil_all)
        } else if let Some(dim) = &opt.dim {
            let mut sp = dim.split('x');
            let dx = sp
                .next()
                .ok_or_else(|| anyhow!("cannot parse dimensions"))?
                .parse()?;
            let dy = sp
                .next()
                .ok_or_else(|| anyhow!("cannot parse dimensions"))?
                .parse()?;
            if sp.next().is_some() {
                return Err(anyhow!("cannot parse dimensions"));
            }
            Grid::new(types, dx, dy)
        } else {
            return Err(anyhow!("need either a dimension or a stencil"));
        };
        let (width, height) = grid.dim();
        let mut collapser = match opt.schedule {
            CollapseSchedule::LeastEntropy => Collapser::least_entropy(),
            CollapseSchedule::OffsetScan => Collapser::offset_scan(width, height),
            CollapseSchedule::Quadtree => Collapser::quadtree(width, height),
        };
        let mut pc = grid.make_propagation_context();
        let mut frame = 0;
        let mut i = 0;
        while let Some(point) = collapser.next(&mut rng, &grid) {
            match grid.collapse_at(&mut pc, &mut rng, point) {
                Ok(modified) => {
                    if modified {
                        if opt.output_frames {
                            if opt.verbose {
                                eprintln!(" frame {frame} at iteration {i}");
                            }
                            let out = tileset.grid_to_image(&fill, &grid, true)?;
                            let filename = format!("frames/{frame:0>4}.png");
                            out.save(filename)?;
                            frame += 1;
                        } else {
                            if opt.verbose {
                                eprintln!(" change at iteration {i}");
                            }
                        }
                    }
                }
                Err(_) => {
                    attempt += 1;
                    break;
                }
            }
            i += 1;
        }
        if let Ok(out) = tileset.grid_to_image(&fill, &grid, false) {
            out.save(&opt.out)?;
            eprintln!("success!");
            break;
        } else {
            eprintln!("failed");
            if !opt.retry {
                break;
            }
        }
    }
    Ok(())
}
