use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

use anyhow::anyhow;
use image::{GenericImage, GenericImageView, RgbaImage};

use super::{
    tiles::{TileTypes, Tiles},
    Grid,
};

#[derive(Debug, Clone)]
pub struct Tile {
    pub x: u32,
    pub y: u32,
    pub edges: [usize; 4],
}

#[derive(Debug, Clone)]
pub struct Tileset {
    pub img: RgbaImage,
    pub tile_size: u32,
    pub tiles: Vec<Tile>,
}

impl Tileset {
    pub fn from_file<P: AsRef<Path>>(path: P) -> anyhow::Result<Tileset> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let mut offset = (0, 0);
        let mut tile_size = 16;
        let mut tiles = Vec::new();
        let mut img_opt = None;
        fn register_edge<'a>(registry: &mut Vec<String>, edge: &'a str) -> usize {
            if let Some(eid) = registry.iter().position(|e| e == edge) {
                eid
            } else {
                let eid = registry.len();
                registry.push(edge.to_string());
                eid
            }
        }
        let mut edge_registry = Vec::new();
        for line in reader.lines() {
            let line = line?;
            if line.is_empty() {
                continue;
            }
            let mut sp = line.split(' ');
            let cmd = sp.next().unwrap();
            match cmd {
                "S" => {
                    tile_size = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?
                        .parse()?;
                }
                "I" => {
                    let path = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?;
                    img_opt = Some(image::open(path)?.to_rgba8());
                }
                "O" => {
                    let x = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?
                        .parse()?;
                    let y = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?
                        .parse()?;
                    offset = (x, y);
                }
                "T" => {
                    let x: u32 = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?
                        .parse()?;
                    let y: u32 = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?
                        .parse()?;
                    let ent = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?;
                    let eet = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?;
                    let est = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?;
                    let ewt = sp
                        .next()
                        .ok_or_else(|| anyhow!("not enough arguments to command"))?;
                    let en = register_edge(&mut edge_registry, ent);
                    let ee = register_edge(&mut edge_registry, eet);
                    let es = register_edge(&mut edge_registry, est);
                    let ew = register_edge(&mut edge_registry, ewt);
                    let edges = [en, ee, es, ew];
                    tiles.push(Tile {
                        x: x + offset.0,
                        y: y + offset.1,
                        edges,
                    });
                }
                _ => {
                    // error?
                }
            }
        }
        Ok(Tileset {
            img: img_opt.ok_or_else(|| anyhow!("no image specified in tileset"))?,
            tile_size,
            tiles,
        })
    }

    pub fn make_tile_types(&self) -> TileTypes {
        fn opposite_edge(idx: usize) -> usize {
            (idx + 2) % 4
        }
        let mut types = TileTypes::empty(self.tiles.len());
        for (src_idx, src_tile) in self.tiles.iter().enumerate() {
            for edge_idx in 0..4 {
                for (tg_idx, tg_tile) in self.tiles.iter().enumerate() {
                    if src_tile.edges[edge_idx] == tg_tile.edges[opposite_edge(edge_idx)] {
                        let ae = &mut types[src_idx].acceptable[edge_idx];
                        *ae = Tiles::union(*ae, Tiles::singleton(tg_idx));
                    }
                }
            }
        }
        types
    }

    pub fn grid_to_image(
        &self,
        fill: &RgbaImage,
        grid: &Grid,
        allow_superpositions: bool,
    ) -> anyhow::Result<RgbaImage> {
        let (gw, gh) = grid.dim();
        let (width, height) = (gw as u32 * self.tile_size, gh as u32 * self.tile_size);
        let mut img = RgbaImage::new(width, height);
        for iy in 0..gh {
            for ix in 0..gw {
                let tiles = grid[(ix, iy)];
                if let Some(tile_idx) = tiles.get_idx() {
                    let tile_info = &self.tiles[tile_idx];
                    let tile_view = self.img.view(
                        tile_info.x * self.tile_size,
                        tile_info.y * self.tile_size,
                        self.tile_size,
                        self.tile_size,
                    );
                    img.copy_from(
                        &*tile_view,
                        ix as u32 * self.tile_size,
                        iy as u32 * self.tile_size,
                    )?;
                } else if allow_superpositions {
                    img.copy_from(fill, ix as u32 * self.tile_size, iy as u32 * self.tile_size)?;
                } else {
                    return Err(anyhow!("no superpositions allowed in output image"));
                }
            }
        }
        Ok(img)
    }
}
