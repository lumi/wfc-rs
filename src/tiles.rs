use core::{
    fmt,
    ops::{Index, IndexMut},
};

use rand::Rng;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Tiles(u64);

impl fmt::Debug for Tiles {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut rest = self.0;
        write!(f, "Tile(")?;
        let mut i = 0;
        let mut first = true;
        while rest != 0 {
            if rest & 1 != 0 {
                if first {
                    write!(f, "{}", i)?;
                    first = false;
                } else {
                    write!(f, " {}", i)?;
                }
            }
            rest = rest >> 1;
            i += 1;
        }
        write!(f, ")")
    }
}

impl Tiles {
    pub const ALL: Tiles = Tiles(u64::MAX);
    pub const NONE: Tiles = Tiles(0);

    pub fn truncate(self, len: usize) -> Tiles {
        Tiles(self.0 & (u64::MAX >> (64 - len as u64)))
    }

    pub fn len(self) -> usize {
        self.0.count_ones() as usize
    }

    pub fn intersect(self, other: Tiles) -> Tiles {
        Tiles(self.0 & other.0)
    }

    pub fn union(self, other: Tiles) -> Tiles {
        Tiles(self.0 | other.0)
    }

    pub fn singleton(idx: usize) -> Tiles {
        Tiles(1 << idx as u64)
    }

    pub fn without(self, other: Tiles) -> Tiles {
        Tiles(self.0 - (self.0 & other.0))
    }

    pub fn get_idx(self) -> Option<usize> {
        let len = self.len();
        if len == 0 {
            return None;
        }
        if len > 1 {
            return None;
        }
        let mut rest = self.0;
        let mut i = 0;
        while rest != 0 {
            if rest & 1 != 0 {
                return Some(i);
            }
            rest = rest >> 1;
            i += 1;
        }
        None
    }

    pub fn random_idx<R: Rng>(self, rng: &mut R) -> Option<usize> {
        let len = self.len();
        if len == 0 {
            return None;
        }
        let idx = rng.gen_range(0..=len);
        let mut rest = self.0;
        let mut i = 0;
        let mut j = 0;
        while rest != 0 {
            if rest & 1 != 0 {
                j += 1;
                if j >= idx {
                    return Some(i);
                }
            }
            rest = rest >> 1;
            i += 1;
        }
        unreachable!()
    }

    pub fn iter(self) -> TilesIter {
        TilesIter { i: 0, rest: self.0 }
    }
}

pub struct TilesIter {
    i: usize,
    rest: u64,
}

impl Iterator for TilesIter {
    type Item = usize;
    fn next(&mut self) -> Option<usize> {
        loop {
            if self.rest == 0 {
                break None;
            }
            let i = self.i;
            let rest = self.rest;
            self.rest = self.rest >> 1;
            self.i += 1;
            if rest & 1 != 0 {
                break Some(i);
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct TileTypes {
    types: Vec<TileType>,
}

impl TileTypes {
    pub fn empty(count: usize) -> TileTypes {
        let types = vec![TileType::empty(); count];
        TileTypes { types }
    }

    pub fn count(&self) -> usize {
        self.types.len()
    }
}

impl Index<usize> for TileTypes {
    type Output = TileType;

    fn index(&self, idx: usize) -> &TileType {
        &self.types[idx]
    }
}

impl IndexMut<usize> for TileTypes {
    fn index_mut(&mut self, idx: usize) -> &mut TileType {
        &mut self.types[idx]
    }
}

#[derive(Debug, Clone)]
pub struct TileType {
    pub acceptable: [Tiles; 4], // N E S W
}

impl TileType {
    pub fn empty() -> TileType {
        TileType {
            acceptable: [Tiles::NONE; 4],
        }
    }
}

#[test]
fn tiles_random_idx_singleton() {
    let mut rng = rand::thread_rng();
    for i in 0..64 {
        let tiles = Tiles::singleton(i);
        let out_idx = tiles.random_idx(&mut rng).unwrap();
        assert_eq!(out_idx, i);
    }
}
