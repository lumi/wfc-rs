S 16
I assets/tileset.png

L E empty_space
T 0 0 E E E E

L B blue_pipe
O 0 6
T 0 0 E B E B
T 1 0 B E B E
T 1 2 E B B E
T 2 2 E E B B
T 1 3 B B E E
T 2 3 B E E B

L G green_pipe
O 3 6
T 0 0 E G E G
T 1 0 G E G E
T 1 2 E G G E
T 2 2 E E G G
T 1 3 G G E E
T 2 3 G E E G

L P purple_pipe
O 6 6
T 0 0 E P E P
T 1 0 P E P E
T 1 2 E P P E
T 2 2 E E P P
T 1 3 P P E E
T 2 3 P E E P
